import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import threedview from "../views/3dView.vue";

import mload from "../views/MLoad.vue";
import marble from "../views/Marble.vue";
import marblecard from "../views/MarbleCard.vue";
import marbleicon from "../views/MarbleIcon.vue";

import tiles from "../views/Tiles.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/3d-view",
    name: "cenas",
    component: threedview
  },
  {
    path: "/3d-mload",
    name: "mload",
    component: mload
  },
  {
    path: "/marble",
    name: "marble",
    component: marble
  },
  {
    path: "/marble-card",
    name: "marble-card",
    component: marblecard
  },
  {
    path: "/marble-icon",
    name: "marble-icon",
    component: marbleicon
  },
  {
    path: "/tiles",
    name: "tiles",
    component: tiles
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
